;;; publish.el --- Publish reveal.js presentation from Org files on Gitlab Pages
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; Copyright (C) 2017, 2018, 2019 Jens Lechtenbörger

;;; License: GPLv3

;;; Commentary:
;; Inspired by publish.el by Rasmus:
;; https://gitlab.com/pages/org-mode/blob/master/publish.el


;;; Code:
(package-initialize)
(require 'org)
(require 'ox-publish)

(setq debug-on-error t org-export-with-smart-quotes t
      org-confirm-babel-evaluate nil)

(add-to-list 'load-path
	     (expand-file-name
	      "../emacs-reveal/" (file-name-directory load-file-name)))
(require 'emacs-reveal)
(setq org-revealjs-root "./reveal.js"
      emacs-reveal-latex-figure-float "H")

;; Following colors are based on tango custom theme.
(custom-set-faces
 '(default                      ((t (:foreground "#2e3436"))))
 '(font-lock-builtin-face       ((t (:foreground "#75507b"))))
 '(font-lock-comment-face       ((t (:foreground "#5f615c"))))
 '(font-lock-constant-face      ((t (:foreground "#204a87"))))
 '(font-lock-function-name-face ((t (:bold t :foreground "#a40000"))))
 '(font-lock-keyword-face       ((t (:foreground "#346604"))))
 '(font-lock-string-face        ((t (:foreground "#5c3566"))))
 '(font-lock-type-face          ((t (:foreground "#204a87"))))
 '(font-lock-variable-name-face ((t (:foreground "#b35000"))))
 )

;; Use markup for meta-data on index page.
(setq org-html-doctype "html5"
      org-html-postamble
      "<p class=\"author\">Lizenz: Der Text „<span property=\"dc:title\">%t</span>“ von <span property=\"dc:creator cc:attributionName\">%a</span> ist unter der Creative-Commons-Lizenz <a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\">CC BY-SA 4.0</a>veröffentlicht.  <a href=\"https://gitlab.com/oer/fediverse/\">Quelldateien</a>.</p>
<p class=\"date\">Erzeugt: <span property=\"dc:created\">%C</span></p>
<div class=\"legalese\"><p><a href=\"/imprint.html\">Impressum</a> | <a href=\"/privacy-de.html\">Datenschutz</a></p></div>")

;; For self-contained presentations that can be used offline, the file
;; index.css needs to be available.  That file is available at
;; oer.gitlab.io.  To embed here, different alternatives could be
;; used.  Embedding in every HTML head causes larger HTML files with
;; redundant contents.  Thus, download once and publish.
;; Link from Org file if necessary.
(require 'url)
(unless (file-exists-p "index.css") ; Avoid download in case of local copy.
  (url-copy-file "https://oer.gitlab.io/index.css" "index.css"))

(setq org-latex-pdf-process
      '("latexmk -outdir=%o -interaction=nonstopmode -shell-escape -bibtex -pdf %f")
      org-publish-project-alist
      (list
       (list "Course"
       	     :base-directory "."
       	     :base-extension "org"
       	     :exclude "index\\|backmatter\\|config\\|course-list\\|license-template\\|literatur"
       	     :publishing-function '(org-revealjs-publish-to-reveal
       				    org-latex-publish-to-pdf)
       	     :publishing-directory "./public")
       (list "index"
	     :base-directory "."
	     :include '("index.org")
	     :exclude ".*"
	     :publishing-function '(org-html-publish-to-html)
	     :publishing-directory "./public")
       (list "index-css"
	     :base-directory "."
	     :include '("index.css")
	     :exclude ".*"
	     :publishing-function '(org-publish-attachment)
	     :publishing-directory "./public")
       (list "figures"
	     :base-directory "figures"
	     :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	     :publishing-directory "./public/figures"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "audios"
	     :base-directory "audio"
	     :base-extension (regexp-opt '("ogg"))
	     :publishing-directory "./public/audio"
	     :publishing-function 'org-publish-attachment)
       (list "quizzes"
	     :base-directory "quizzes"
	     :base-extension (regexp-opt '("js"))
	     :publishing-directory "./public/quizzes"
	     :publishing-function 'org-publish-attachment)
       (list "texts"
       	     :base-directory "texts"
       	     :base-extension "org"
	     :exclude "config"
       	     :publishing-function '(org-latex-publish-to-pdf
				    org-html-publish-to-html)
       	     :publishing-directory "./public")
       (list "title-slide"
	     :base-directory "emacs-reveal/title-slide"
	     :base-extension (regexp-opt '("png" "jpg" "svg"))
	     :publishing-directory "./public/title-slide/"
	     :publishing-function 'org-publish-attachment)
       (list "title-logos"
	     :base-directory "non-free-logos/title-slide"
	     :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	     :publishing-directory "./public/title-slide"
	     :publishing-function 'org-publish-attachment)
       (list "theme-logos"
	     :base-directory "non-free-logos/reveal-css"
	     :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	     :publishing-directory "./public/reveal.js/css/theme"
	     :publishing-function 'org-publish-attachment)
       (list "reveal-static"
	     :base-directory "emacs-reveal/reveal.js"
	     :exclude "\\.git"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal-theme"
	     :base-directory "emacs-reveal/css"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/css/theme"
	     :publishing-function 'org-publish-attachment)
       (list "reveal-toc-plugin"
	     :base-directory "emacs-reveal/Reveal.js-TOC-Progress/plugin"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-plugins-anything"
	     :base-directory "emacs-reveal/reveal.js-plugins/anything"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/anything"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-plugins-audio-slideshow"
	     :base-directory "emacs-reveal/reveal.js-plugins/audio-slideshow"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/audio-slideshow"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-jump-plugin"
	     :base-directory "emacs-reveal/reveal.js-jump-plugin/jump"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/jump"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       (list "reveal.js-quiz-plugin"
	     :base-directory "emacs-reveal/reveal.js-quiz/quiz"
	     :base-extension 'any
	     :publishing-directory "./public/reveal.js/plugin/quiz"
	     :publishing-function 'org-publish-attachment
	     :recursive t)
       ;; (list "mathjax-static"
       ;; 	     :base-directory "../emacs-reveal/MathJax"
       ;; 	     :exclude "\\.git"
       ;; 	     :base-extension 'any
       ;; 	     :publishing-directory "./public/MathJax"
       ;; 	     :publishing-function 'org-publish-attachment
       ;; 	     :recursive t)
       ))

(provide 'publish)
;;; publish.el ends here
