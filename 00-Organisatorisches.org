# Local IspellDict: de
#+STARTUP: showeverything
#+INCLUDE: config.org

#+TITLE: Organisatorisches
#+DATE: Kommunikation im Fediverse 2018/19
#+AUTHOR: Jens Lechtenbörger
#+REVEAL_ACADEMIC_TITLE: Dr.

* Anforderungen
** VM Laut Modulhandbuch
   - Vorlesungsteil mit mündlicher Prüfung
     - Ein Drittel der Gesamtnote
   - Seminarteil mit Ausarbeitung und Vortrag
     - Zwei Drittel der Gesamtnote
     - Ausarbeitung 45%, Vortrag 45%, Diskussion 10%
   - *Vorgezogene Anmeldung* beim Prüfungsamt erforderlich!

** Ausarbeitung
   - Wissenschaftliche Ausarbeitung, 10-15 Seiten
     - ~guide2seminars.pdf~ aus Vorlage beachten!
       - Hinweise am 15.11.
     - Weitere Hinweise
       - Ggf. strukturierte Literatursuche
       - Verwendung von LaTeX optional, aber empfohlen

* Termine
** Modus der „Vorlesungen“
   - Ihre Vorbereitung ist notwendig
     - Instruktionen in Learnweb
   - Diskussionen und Übungen an Präsenzterminen

** Präsenztermine
   - Dienstags, PC-Pool, 14-16 Uhr am 16.10., 23.10., 30.10, 6.11.
   - Donnerstags, Leo 2, 14-16 Uhr am 18.10., 25.10., 8.11.
   - Donnerstag, 15.11., 14-16 Uhr, Raum 006 im *Leonardo-Campus 11*

** Weitere Termine
   - Wann mündliche Prüfungen?
   - Wann Seminarvorträge?

* Inhalte
** VL-Themen
   - Heute: Organisatorisches und Grundbegriffe
   - 18.10. Internet
   - 23.10. DNS
   - 25.10. Zertifikate und PKI
   - 30.10. Fediverse-Protokolle
   - 06.11. Blockchain
   - 08.11. Weiteres Projekt,
     z. B. [[https://solid.inrupt.com/][Solid]]
     oder Suche ([[https://yacy.net/de/][YaCy]], [[https://www.searx.me/][Searx]])
   - 15.11. Seminarvorbereitung, „Speed Dating“, gemeinsame Anforderungen
   - Ihre Wünsche?

#+MACRO: copyrightyears 2018
#+INCLUDE: license-template-de.org

# Local Variables:
# indent-tabs-mode: nil
# End:
