<!--- Local IspellDict: de -->

Dieses Projekt sammelt
[offene Bildungsmaterialien](https://de.wikipedia.org/wiki/Open_Educational_Resources)
(Open Educational Resources, OER) und Verweise zu Literatur rund um
das Thema Fediverse.

[Präsentationen und Verweise](https://oer.gitlab.io/fediverse).
