# Local IspellDict: de
#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="index.css" />
#+LANGUAGE: de-de
#+TITLE: OER für das Vertiefungsmodul Kommunikation im Fediverse
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil

* Worum geht es?
Diese Seite sammelt
[[https://de.wikipedia.org/wiki/Open_Educational_Resources][offene Bildungsmaterialien]]
(Open Educational Resources, OER) und Verweise zu Lehr- und Lernmaterialien,
die im Rahmen des Vertiefungsmoduls
[[https://www.wi.uni-muenster.de/de/studierende/lehrangebot/274163][Kommunikation im Fediverse]]
an der WWU Münster genutzt werden.

* Hinweise zu Reveal.js-Präsentationen
  :PROPERTIES:
  :CUSTOM_ID: hints
  :END:
#+INCLUDE: "emacs-reveal/org/reveal.js-hints-de.org"

* Präsentationen und andere Materialien
  :PROPERTIES:
  :CUSTOM_ID: presentations
  :END:
  Materialien werden hier im Laufe des Semesters veröffentlicht.
  Die HTML- und PDF-Versionen werden aus Org-Quelldateien generiert.

  - [[file:00-Organisatorisches.org][Organisatorisches]] ([[file:00-Organisatorisches.pdf][PDF]])
  - [[file:01-Grundbegriffe.org][Grundbegriffe]] ([[file:01-Grundbegriffe.pdf][PDF]])
  - [[file:../DS/DS02-Internet.org][Internet]] (Englisch)
  - [[file:../DS/DNS.org][DNS]] (Englisch)
  - [[file:Fediverse-Protokolle.org][Protokolle im Fediverse]] ([[file:Fediverse-Protokolle.pdf][PDF]])
  - [[file:Bitcoin.org][Bitcoin]] ([[file:Bitcoin.pdf][PDF]])
  - [[file:Suche.org][Suche]] ([[file:Suche.pdf][PDF]])
  Die Materialien können in der Regel auch offline verwendet werden.  Bei Fragen
  helfe ich gerne.

* Quelldateien und Lizenzen
#+MACRO: gitlablink [[https://gitlab.com/oer/fediverse/][$1]]
#+INCLUDE: "emacs-reveal/org/source-code-licenses-de.org"
