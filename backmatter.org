# Copyright (C) 2018-2019 Jens Lechtenbörger
# SPDX-License-Identifier: CC0-1.0

#+INCLUDE: "literatur.org" :minlevel 2
#+INCLUDE: "license-template-de.org" :minlevel 2
